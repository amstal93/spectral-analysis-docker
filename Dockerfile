FROM r-base:3.6.1

# install libs
RUN apt-get update && \
apt-get -y install \
r-base-dev \
libsndfile1-dev \
libsbsms-dev \
libfftw3-dev \
sox \
libsox-fmt-mp3 \
nano \
libssl-dev \
libcurl4-openssl-dev \
libxml2-dev

# install R packages
RUN \
Rscript -e "install.packages('sound')"; \
Rscript -e "install.packages('tuneR')"; \
Rscript -e "install.packages('seewave')"; \
Rscript -e "install.packages('ggplot2')"; \
Rscript -e "install.packages('cowplot')";

