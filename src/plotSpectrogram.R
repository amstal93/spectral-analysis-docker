#!/usr/bin/env Rscript
library(sound)
library(tuneR)
library(seewave)

# get the command-line arguments
args = commandArgs(trailingOnly=TRUE)

if (length(args)!=2) {
  stop("Input and output files must be supplied.n", call.=FALSE)
}

# read the audio file
s1<-readWave(args[1])

# plot the spectrogram and save it
png(args[2],width=1000,height=500)
spectro(s1, palette=reverse.gray.colors.1, scale=FALSE,osc=TRUE,collevels=seq(-80,0,1))
dev.off()
