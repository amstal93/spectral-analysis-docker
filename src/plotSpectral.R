#!/usr/bin/env Rscript
library(sound)
library(tuneR)
library(seewave)

# get the command-line arguments
args = commandArgs(trailingOnly=TRUE)

if (length(args)!=2) {
  stop("Input and output files must be supplied.n", call.=FALSE)
}

# read the audio file
s1<-readWave(args[1])

# plot the spectrogram and save it
png(args[2],width=1000,height=500)
spec(s1, f=22050)
dev.off()
