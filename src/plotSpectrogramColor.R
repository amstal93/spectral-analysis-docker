#!/usr/bin/env Rscript
library(sound)
library(tuneR)
library(seewave)

# get the command-line arguments
args = commandArgs(trailingOnly=TRUE)

if (length(args)!=2) {
  stop("Input and output files must be supplied.n", call.=FALSE)
}

# read the audio file
s1<-readWave(args[1])

# plot the spectrogram and save it
png(args[2],width=1000,height=500)
spectro(s1, f=22050, ovlp=50, zp=16, collevels=seq(-60,0,0.5), scale=FALSE,osc=TRUE)
dev.off()
