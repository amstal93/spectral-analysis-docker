# Prepare audio files
.PHONY: clean prepare prepare_audio example

# example audio file
AUDIO = example

# name of the container
R_CONTAINER=spectral

# mount point of example dir on the container
W_DIR=/example

example: | clean spectro
	echo "Done generating spectrogram"

# draw the spectrograms
spectro: wav
	docker exec -ti ${R_CONTAINER} script -q -c " \
	mkdir -p spectro"
	$(foreach var, ${AUDIO}, \
	docker exec -ti ${R_CONTAINER} script -q -c "\
		$(eval CURRENT_FILE:=${var}) $(draw_spc_cmd); \
		$(eval CURRENT_FILE:=${var}) $(draw_osc_cmd); \
		$(eval CURRENT_FILE:=${var}) $(draw_spca_cmd); \
		$(eval CURRENT_FILE:=${var}) $(draw_spcolor_cmd);" \
	)

# generate 1 minute of silence to pad short files
# convert each file in AUDIO
# remove the silence file
wav:
	docker exec -ti ${R_CONTAINER} script -q -c " \
	sox -n -r 22050 -c 1 ${W_DIR}/silence.wav trim 0 60"
	$(foreach var, ${AUDIO}, \
	docker exec -ti ${R_CONTAINER} script -q -c \
		"$(eval CURRENT_FILE:=${var}) $(convert_audio_cmd);" \
	)

# cleanup output and temp files for example
clean:
	docker exec -ti ${R_CONTAINER} script -q -c " \
	rm -rf ${W_DIR}/example.wav ${W_DIR}/example.png /workspace/typescript\
	"

###################################
#       Commands
###################################

# Call sox to convert mp3 to wav and trim it
# convert the mp3 to wav
# pad with silence and store in temp file
# trim from the beginning to MAX_AUDIO_LENGTH
# remove the temp file
MAX_AUDIO_LENGTH = 20 # seconds
define convert_audio_cmd
	sox ${W_DIR}/${CURRENT_FILE}.mp3 -r 22050 -c 1 ${W_DIR}/${CURRENT_FILE}.wav; \
	sox ${W_DIR}/${CURRENT_FILE}.wav ${W_DIR}/silence.wav ${W_DIR}/${CURRENT_FILE}_temp.wav; \
	sox ${W_DIR}/${CURRENT_FILE}_temp.wav ${W_DIR}/${CURRENT_FILE}.wav trim 0 ${MAX_AUDIO_LENGTH}; \
	rm ${W_DIR}/${CURRENT_FILE}_temp.wav
endef

# Call R to plot a spectrogramm
define draw_spc_cmd
	Rscript /src/plotSpectrogram.R ${W_DIR}/${CURRENT_FILE}.wav ${W_DIR}/${CURRENT_FILE}_spc.png 
endef

# Call R to plot an oscillogramm
define draw_osc_cmd
	Rscript /src/plotOscillogram.R ${W_DIR}/${CURRENT_FILE}.wav ${W_DIR}/${CURRENT_FILE}_osc.png
endef

# Call R to plot a spectral analysis
define draw_spca_cmd
	Rscript /src/plotSpectral.R ${W_DIR}/${CURRENT_FILE}.wav ${W_DIR}/${CURRENT_FILE}_spca.png
endef

# Call R to plot a color-code spectrogramm
define draw_spcolor_cmd
	Rscript /src/plotSpectrogramColor.R ${W_DIR}/${CURRENT_FILE}.wav ${W_DIR}/${CURRENT_FILE}_spcolor.png
endef

